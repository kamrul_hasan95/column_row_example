import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ColumnRow(),
    );
  }
}

class ColumnRow extends StatefulWidget {
  @override
  _ColumnRowState createState() => _ColumnRowState();
}

class _ColumnRowState extends State<ColumnRow> {
  String phoneNumber = '+8801791441686';
  String name = "Kamrul Hasan";
  String email = 'starlut95@gmail.com';
  String address = 'Chuadanga';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My BioData'),
      ),
      body: Container(
        color: Colors.pink,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.network(
              'https://scontent.fcla1-1.fna.fbcdn.net/v/t1.0-9/121488351_3361910197241583_4860489022137038399_o.jpg?_nc_cat=108&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGAkDj0Gg6w1uKlxz-PPyqsn-oJUWLdFYif6glRYt0ViAmZIeOa1g06sVef2_ALGlYUhX2JQgzCU2zjZjMCw8oo&_nc_ohc=jJ6mz9bQZHcAX_XbwN4&_nc_ht=scontent.fcla1-1.fna&oh=e8ff7fb0539af2f9edf6f50e54175dc8&oe=601708F7',
              height: 300,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "Name: " + name,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 32.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Phone Number: ',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 26.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Text(
                    phoneNumber,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 26.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 30.0),
                  child: Text(
                    'Email: $email',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 22.0,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text(
                    'Address: $address',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 22.0,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
